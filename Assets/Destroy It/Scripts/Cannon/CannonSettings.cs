﻿using UnityEngine;

[CreateAssetMenu(fileName = "Cannon Settings", menuName = "DestroyIt/Settings/Cannon")]
public class CannonSettings : ScriptableObject
{
    [SerializeField] private float speedChangingForceMultiplier;
    [SerializeField] private float pushForce;

    public float SpeedChangingForceMultiplier => speedChangingForceMultiplier;
    public float PushForce => pushForce;
}