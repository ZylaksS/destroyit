﻿using UnityEngine;

public class CannonView : MonoBehaviour
{
    [SerializeField] private CannonModel cannonModel;
    [SerializeField] private ForceLine forceLine;

    private void Awake()
    {
        forceLine.OnTapped += StartPreparing;
        forceLine.OnTapHolding += ChangeForceMultiplier;
        forceLine.OnTapReleased += Shoot;
    }

    public void StartPreparing()
    {
        if (cannonModel.State != CannonState.ReadyToShoot) return;

        cannonModel.StartPreparing();
        forceLine.StartMovingLine();
    }

    public void Shoot()
    {
        if (cannonModel.State != CannonState.Preparing) return;

        cannonModel.Shoot();
    }

    public void SetReadyToShoot() => cannonModel.SetReadyToShoot();

    public void ChangeForceMultiplier()
    {
        if (cannonModel.State != CannonState.Preparing) return;

        forceLine.ChangePosition(cannonModel.ChangeForceMultiplier());
    }

    private void OnDestroy()
    {
        if (forceLine == null) return;

        forceLine.OnTapped -= StartPreparing;
        forceLine.OnTapHolding -= ChangeForceMultiplier;
        forceLine.OnTapReleased -= Shoot;
    }
}