﻿using System;
using UnityEngine;

public class CannonModel : MonoBehaviour
{
    [SerializeField] private CannonSettings cannonSettings;

    private const float HalfProgress01 = 0.5f;
    private const float MultiplierForFormula = 2.0f;

    private float forceMultiplier;

    public event Action OnPreparingToShoot;
    public event Action<float> OnShot;
    public event Action OnReloaded;

    public CannonState State { get; private set; } = CannonState.Waiting;

    public void StartPreparing()
    {
        if (State != CannonState.ReadyToShoot) return;

        State = CannonState.Preparing;
        OnPreparingToShoot?.Invoke();
    }

    public void Shoot()
    {
        if (State != CannonState.Preparing) return;

        State = CannonState.Shot;
        OnShot?.Invoke(GetPushForceMultiplier() * cannonSettings.PushForce);

        forceMultiplier = 0;
    }

    public float GetPushForceMultiplier()
    {
        if (forceMultiplier > HalfProgress01)
        {
            return (1 - forceMultiplier) * MultiplierForFormula;
        }
        else
        {
            return forceMultiplier * MultiplierForFormula;
        }
    }

    public void Reload()
    {
        OnReloaded?.Invoke();
    }

    public void SetReadyToShoot()
    {
        State = CannonState.ReadyToShoot;
    }

    public float ChangeForceMultiplier()
    {
        if (State != CannonState.Preparing) return - 1;

        forceMultiplier = Mathf.PingPong(Time.time * cannonSettings.SpeedChangingForceMultiplier, 1);
        return forceMultiplier;
    }
}