﻿public interface IReloadable 
{
    void Reload(IShell shell);
}