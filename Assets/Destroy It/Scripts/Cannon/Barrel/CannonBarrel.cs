﻿using UnityEngine;

public class CannonBarrel : MonoBehaviour, IReloadable
{
    [SerializeField] private CannonModel cannonModel;
    [SerializeField] private Transform shellTargetPosition;
    [SerializeField] private ParticleSystem shootEffect;
    [SerializeField] private Animator animator;
    [SerializeField] private string shootTrigger;

    private IShell shell;

    private void Awake()
    {
        cannonModel.OnShot += Shoot;
    }

    public void Reload(IShell shell)
    {
        this.shell = shell;

        shell.Rigidbody.transform.SetParent(shellTargetPosition, false);
        shell.Rigidbody.transform.localPosition = Vector3.zero;

        cannonModel.Reload();
    }

    public void Shoot(float force)
    {
        shell.Rigidbody.isKinematic = false;
        shell.Rigidbody.AddForce(shell.Rigidbody.transform.forward * force, ForceMode.VelocityChange);

        shootEffect.Play();
        animator.SetTrigger(shootTrigger);
    }

    private void OnDestroy()
    {
        if (cannonModel == null) return;

        cannonModel.OnShot -= Shoot;
    }
}