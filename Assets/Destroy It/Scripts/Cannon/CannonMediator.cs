﻿using UnityEngine;

public class CannonMediator : MonoBehaviour
{
    [SerializeField] private CameraMover cameraMover;
    [SerializeField] private CannonView cannon;

    private void Awake()
    {
        cameraMover.OnMoveToShootEnded += cannon.SetReadyToShoot;
    }

    private void OnDestroy()
    {
        cameraMover.OnMoveToShootEnded -= cannon.SetReadyToShoot;
    }
}