﻿public enum CannonState
{
    Waiting, ReadyToShoot, Preparing, Shot
}