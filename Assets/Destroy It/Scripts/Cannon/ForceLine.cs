﻿using System;
using UnityEngine;

public class ForceLine : MonoBehaviour
{
    [SerializeField] private StackShells stackShells;
    [SerializeField] private ShellHitChecker shellHitChecker;
    [SerializeField] private RectTransform line;
    [SerializeField] private RectTransform arrow;

    private Vector3 upTargetPosition;
    private Vector3 downTargetPosition;

    public event Action OnTapped;
    public event Action OnTapHolding;
    public event Action OnTapReleased;

    private void Awake()
    {
        shellHitChecker.OnShellCracked += TryHideMovingLine;
    }

    private void Start()
    {
        upTargetPosition = new Vector3(0, line.rect.height, 0);
        downTargetPosition = Vector3.zero;
    }

    private void Update()
    {
        HandleInput();
    }

    private void HandleInput()
    {
        if (Input.GetMouseButtonDown(0))
        {
            OnTapped?.Invoke();
        }

        if (Input.GetMouseButton(0))
        {
            OnTapHolding?.Invoke();
        }

        if (Input.GetMouseButtonUp(0))
        {
            OnTapReleased?.Invoke();
        }
    }

    public void StartMovingLine()
    {
        line.gameObject.SetActive(true);
    }

    private void TryHideMovingLine()
    {
        if (stackShells.Shells.Count < 1) return;

        line.gameObject.SetActive(false);
    }

    public void ChangePosition(float time)
    {
        arrow.anchoredPosition = Vector3.Lerp(downTargetPosition, upTargetPosition, time);
    }

    private void OnDestroy()
    {
        if(stackShells != null)
        {
            shellHitChecker.OnShellCracked -= TryHideMovingLine;
        }
    }
}