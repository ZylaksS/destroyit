﻿using UnityEngine;

public class Ground : MonoBehaviour, IObstacle 
{
    [SerializeField] private ObstacleType obstacleType;

    public ObstacleType ObstacleType => obstacleType;
}