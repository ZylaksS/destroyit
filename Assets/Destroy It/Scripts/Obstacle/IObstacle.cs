﻿public interface IObstacle 
{
    ObstacleType ObstacleType { get; }
}