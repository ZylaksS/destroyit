﻿using System;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class BuildingPart : MonoBehaviour, IObstacle
{
    [SerializeField] private ObstacleType obstacleType;

    private Rigidbody rigidbody;

    public ObstacleType ObstacleType => obstacleType;

    public event Action<GameObject> OnTouched;

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    public void Activate() => rigidbody.useGravity = true;

    private void OnCollisionEnter(Collision collision)
    {
        OnTouched?.Invoke(collision.gameObject);
    }
}