﻿using UnityEngine;

public class Building : MonoBehaviour
{
    [SerializeField] private BuildingPart[] parts;

    private void Awake()
    {
        SubscribeToBuildingParts();
    }

    private void ActivatePhysics()
    {
        foreach (var part in parts)
        {
            part.Activate();
            part.OnTouched -= HandleToucher;
        }
    }

    private void HandleToucher(GameObject toucher)
    {
        if (toucher.GetComponent<IDestroyer>() == null) return;

        ActivatePhysics();
    }

    private void SubscribeToBuildingParts()
    {
        foreach(var part in parts)
        {
            part.OnTouched += HandleToucher;
        }
    }

    private void UnsubscribeFromBuildingParts()
    {
        foreach (var part in parts)
        {
            part.OnTouched -= HandleToucher;
        }
    }

    private void OnDestroy()
    {
        UnsubscribeFromBuildingParts();
    }
}