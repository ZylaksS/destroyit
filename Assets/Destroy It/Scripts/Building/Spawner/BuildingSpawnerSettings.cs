﻿using UnityEngine;

[CreateAssetMenu(fileName = "Building Spawner Settings", menuName = "DestroyIt/Settings/Building Spawner")]
public class BuildingSpawnerSettings : ScriptableObject
{
    [SerializeField] private GameObject[] buildings;

    public GameObject GetRandomBuilding() => buildings[Random.Range(0, buildings.Length)];
}