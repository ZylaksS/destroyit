﻿using UnityEngine;

public class BuildingSpawner : MonoBehaviour
{
    [SerializeField] private BuildingSpawnerSettings buildingSpawnerSettings;

    private void Awake()
    {
        Instantiate(buildingSpawnerSettings.GetRandomBuilding(), transform);
    }
}