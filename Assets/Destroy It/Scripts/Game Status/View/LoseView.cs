﻿using UnityEngine;
using UnityEngine.UI;

public class LoseView : MonoBehaviour
{
    [SerializeField] private GameStatus gameStatus;
    [SerializeField] private SceneRestarter sceneRestarter;
    [SerializeField] private GameObject loseWindow;
    [SerializeField] private Button restart;

    private void Awake()
    {
        gameStatus.OnLose += ShowWindow;
        restart.onClick.AddListener(sceneRestarter.Restart);
    }

    private void ShowWindow()
    {
        loseWindow.SetActive(true);
    }

    private void OnDestroy()
    {
        gameStatus.OnLose -= ShowWindow;
    }
}