﻿using UnityEngine;
using UnityEngine.UI;

public class WinView : MonoBehaviour
{
    [SerializeField] private GameStatus gameStatus;
    [SerializeField] private SceneRestarter sceneRestarter;
    [SerializeField] private GameObject winWindow;
    [SerializeField] private Button restart;

    private void Awake()
    {
        gameStatus.OnWin += ShowWindow;
        restart.onClick.AddListener(sceneRestarter.Restart);
    }

    private void ShowWindow()
    {
        winWindow.SetActive(true);
    }

    private void OnDestroy()
    {
        gameStatus.OnWin -= ShowWindow;
    }
}