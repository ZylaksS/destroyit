﻿using System;
using UnityEngine;

public class GameStatus : MonoBehaviour
{
    public event Action OnWin;
    public event Action OnLose;

    public void SetWin()
    {
        OnWin?.Invoke();
    }

    public void SetLose()
    {
        OnLose?.Invoke();
    }
}