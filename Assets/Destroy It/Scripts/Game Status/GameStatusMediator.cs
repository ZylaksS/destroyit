﻿using UnityEngine;

public class GameStatusMediator : MonoBehaviour
{
    [SerializeField] private GameStatus gameStatus;
    [SerializeField] private ShellHitChecker shellHitChecker;
    [SerializeField] private StackShells stackShells;

    private void Awake()
    {
        shellHitChecker.OnShellHitTarget += SetWin;
        shellHitChecker.OnShellCracked += SetLose;
    }

    private void SetLose()
    {
        if (stackShells.Shells.Count > 0) return;

        Unsubscribe();
        gameStatus.SetLose();
    }

    private void SetWin()
    {
        Unsubscribe();
        gameStatus.SetWin();
    }

    private void Unsubscribe()
    {
        shellHitChecker.OnShellCracked -= SetLose;
        shellHitChecker.OnShellHitTarget -= SetWin;
    }

    private void OnDestroy()
    {
        Unsubscribe();
    }
}