﻿using UnityEngine;

public class ReloadingPointChecker : MonoBehaviour
{
    [SerializeField] private ObjectsDragger objectsDragger;
    [SerializeField] private LayerMask layerMask;
    [SerializeField] private Camera gameCamera;

    private void Update()
    {
        CheckReloadingPoint();
    }

    private void CheckReloadingPoint()
    {
        if (objectsDragger.DraggableObject == null) return;

        Ray ray = gameCamera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out var hit, Mathf.Infinity, layerMask))
        {
            ReloadPoint(hit);
        }
    }

    private void ReloadPoint(RaycastHit hit)
    {
        hit.transform.GetComponent<IReloadable>().Reload(objectsDragger.DraggableObject.GetComponent<IShell>());
        objectsDragger.ResetDraggingObject();
    }
}