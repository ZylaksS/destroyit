﻿using UnityEngine;

public class ObjectsDragger : MonoBehaviour
{
    private IDraggable draggableObject;
    private bool isActive = true;

    public Transform DraggableObject
    {
        get
        {
            if (draggableObject == null) return null;

            return draggableObject.DraggableObject;
        }
    }

    public bool IsActive => isActive;

    public bool TryTakeShell(RaycastHit hit)
    {
        draggableObject = hit.transform.GetComponent<IDraggable>();

        return draggableObject != null;
    }

    public void ResetDraggingObject() => draggableObject = null;

    public void Activate() => isActive = true;
    public void Deactivate() => isActive = false;
}