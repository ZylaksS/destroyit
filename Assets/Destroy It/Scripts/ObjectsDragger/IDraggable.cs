﻿using UnityEngine;

public interface IDraggable
{
    Transform DraggableObject { get; }
}