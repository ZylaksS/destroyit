﻿using UnityEngine;

public class ObjectsDraggerView : MonoBehaviour
{
    [SerializeField] private ObjectsDragger objectsDragger;
    [SerializeField] private Camera gameCamera;

    private Vector3 mouseOffset;
    private float mouseZCoord;

    private void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            TryTakeShell();
        }

        if(Input.GetMouseButton(0))
        {
            DragShell();
        }

        if(Input.GetMouseButtonUp(0))
        {
            ResetDragShell();
        }
    }

    public void TryTakeShell()
    {
        if (objectsDragger.IsActive == false) return;

        Ray ray = gameCamera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out var hit))
        {
            if(objectsDragger.TryTakeShell(hit))
            {
                mouseZCoord = gameCamera.WorldToScreenPoint(objectsDragger.DraggableObject.position).z;
                mouseOffset = objectsDragger.DraggableObject.position - GetMouseWorldPosition();
            }
        }
    }

    private void DragShell()
    {
        if (objectsDragger.DraggableObject == null) return;

        objectsDragger.DraggableObject.position = GetMouseWorldPosition() + mouseOffset;
    }

    private void ResetDragShell()
    {
        objectsDragger.ResetDraggingObject();
    }

    public Vector3 GetMouseWorldPosition()
    {
        Vector3 mousePosition = Input.mousePosition;
        mousePosition.z = mouseZCoord;

        return gameCamera.ScreenToWorldPoint(mousePosition);
    }
}