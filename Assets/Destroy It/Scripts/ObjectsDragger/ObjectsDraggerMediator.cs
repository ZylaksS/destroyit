﻿using UnityEngine;

public class ObjectsDraggerMediator : MonoBehaviour
{
    [SerializeField] private ObjectsDragger objectsDragger;
    [SerializeField] private CameraMover cameraMover;
    [SerializeField] private CannonModel cannonModel;

    private void Awake()
    {
        cannonModel.OnReloaded += objectsDragger.Deactivate;
        cameraMover.OnMoveToReloadEnded += objectsDragger.Activate;
    }

    private void OnDestroy()
    {
        cannonModel.OnReloaded -= objectsDragger.Deactivate;
        cameraMover.OnMoveToReloadEnded -= objectsDragger.Activate;
    }
}