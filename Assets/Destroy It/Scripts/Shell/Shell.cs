﻿using System;
using UnityEngine;

public class Shell : MonoBehaviour, IDestroyer, IDraggable, IShell
{
    [SerializeField] private Rigidbody rigidbody;

    private bool isContacted;

    public event Action<Shell> OnUsed;
    public event Action<IObstacle> OnHit;

    public Transform DraggableObject => transform;
    public Rigidbody Rigidbody => rigidbody;

    private void OnCollisionEnter(Collision collision)
    {
        if (isContacted) return;

        var obstacle = collision.gameObject.GetComponent<IObstacle>();

        if (obstacle == null) return;

        isContacted = true;

        OnUsed?.Invoke(this);
        OnHit?.Invoke(obstacle);

        Destroy(gameObject);
    }
}
