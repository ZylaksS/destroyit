﻿using System;
using UnityEngine;

public class ShellHitChecker : MonoBehaviour
{
    [SerializeField] private StackShells stackShells;

    public event Action OnShellCracked;
    public event Action OnShellHitTarget;

    private void Awake()
    {
        foreach(var shell in stackShells.Shells)
        {
            shell.OnHit += HandleObstacle;
        }
    }

    private void HandleObstacle(IObstacle obstacle)
    {
        switch(obstacle.ObstacleType)
        {
            case ObstacleType.Win:
                OnShellHitTarget?.Invoke();
                break;

            case ObstacleType.Lose:
                OnShellCracked?.Invoke();
                break;
        }
    }

    private void OnDestroy()
    {
        foreach (var shell in stackShells.Shells)
        {
            shell.OnHit -= HandleObstacle;
        }
    }
}