﻿using System.Collections.Generic;
using UnityEngine;

public class StackShells : MonoBehaviour
{
    [SerializeField] private List<Shell> shells;

    public List<Shell> Shells => shells;

    private void Awake()
    {
        foreach (var shell in shells)
        {
            shell.OnUsed += RemoveShellFromStack;
        }
    }

    private void RemoveShellFromStack(Shell shell)
    {
        shells.Remove(shell);
    }

    private void OnDestroy()
    {
        foreach (var shell in shells)
        {
            shell.OnUsed -= RemoveShellFromStack;
        }
    }
}