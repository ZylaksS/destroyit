﻿using UnityEngine;

public interface IShell 
{
    Rigidbody Rigidbody { get; }
}