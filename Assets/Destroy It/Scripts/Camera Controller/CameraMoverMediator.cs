﻿using UnityEngine;

public class CameraMoverMediator : MonoBehaviour
{
    [SerializeField] private CannonModel cannon;
    [SerializeField] private ShellHitChecker shellHitChecker;
    [SerializeField] private StackShells stackShells;
    [SerializeField] private CameraMover cameraMover;

    private void Awake()
    {
        cannon.OnReloaded += cameraMover.MoveToShoot;
        shellHitChecker.OnShellCracked += MoveToReload;
    }

    private void MoveToReload()
    {
        if (stackShells.Shells.Count < 1) return;

        cameraMover.MoveToReload();
    }

    private void OnDestroy()
    {
        cannon.OnReloaded -= cameraMover.MoveToShoot;
        shellHitChecker.OnShellCracked -= MoveToReload;
    }
}