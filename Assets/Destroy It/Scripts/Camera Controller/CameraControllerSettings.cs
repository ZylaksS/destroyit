﻿using UnityEngine;

[CreateAssetMenu(fileName = "Camera Controller Settings", menuName = "DestroyIt/Settings/Camera Controller")]
public class CameraControllerSettings : ScriptableObject
{
    [SerializeField] private float moveToReloadTime;
    [SerializeField] private float moveToShootTime;

    public float MoveToReloadTime => moveToReloadTime;
    public float MoveToShootTime => moveToShootTime;
}