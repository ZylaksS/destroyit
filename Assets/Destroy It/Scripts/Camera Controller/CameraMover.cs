﻿using System;
using System.Collections;
using UnityEngine;

public class CameraMover : MonoBehaviour
{
    [SerializeField] private CameraControllerSettings cameraControllerSettings;
    [SerializeField] private Transform gameCamera;
    [SerializeField] private Transform reloadingPoint;
    [SerializeField] private Transform shootPoint;

    private Coroutine moveCoroutine;

    public event Action OnMoveToShootEnded;
    public event Action OnMoveToReloadEnded;

    public void MoveToShoot()
    {
        if (moveCoroutine != null) return;

        moveCoroutine = StartCoroutine(Move(shootPoint, cameraControllerSettings.MoveToShootTime, OnMoveToShootEnded));
    }

    public void MoveToReload()
    {
        if (moveCoroutine != null) return;

        moveCoroutine = StartCoroutine(Move(reloadingPoint, cameraControllerSettings.MoveToReloadTime, OnMoveToReloadEnded));
    }

    private IEnumerator Move(Transform point, float time, Action action)
    {
        Vector3 startPosition = gameCamera.position;
        Quaternion startRotation = gameCamera.rotation;
        var timer = 0.0f;
        var currentTime = 0.0f;

        while(timer < time)
        {
            timer += Time.deltaTime;
            currentTime = timer / time;

            gameCamera.position = Vector3.Lerp(startPosition, point.position, currentTime);
            gameCamera.rotation = Quaternion.Lerp(startRotation, point.rotation, currentTime);

            yield return null;
        }

        action?.Invoke();

        moveCoroutine = null;
    }
}